export class ParkingLot{
    vehicle: string
    plate: string
    driver: string
    phone: number
    endTime: Date
    price: number


    constructor(vehicle: string, plate: string, driver: string, phone: number){
        this.vehicle = vehicle
        this.plate = plate
        this.driver = driver
        this.phone = phone
        this.endTime = new Date
        this.price = 4.5

    }
}