import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TelaAComponent } from './tela-a.component';

describe('TelaAComponent', () => {
  let component: TelaAComponent;
  let fixture: ComponentFixture<TelaAComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TelaAComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TelaAComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
