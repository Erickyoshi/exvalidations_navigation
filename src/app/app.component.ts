import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {Validators, FormControl, FormBuilder, FormGroup } from "@angular/Forms";
import { ParkingLot } from './model/parkinglot.model';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  parkingLotForm = this.formBuilder.group({
    vehicleParking: ['', [Validators.required] ],
    plateParking: ['', [Validators.required, Validators.maxLength(7), Validators.minLength(7)] ],
    driverParking: ['', [Validators.required] ],
    phoneParking: ['', [Validators.required] ]

  })

  allVehicles: ParkingLot [] = []

  constructor(private router: Router, private formBuilder: FormBuilder) { }

  ngOnInit(){
    let list = localStorage.getItem('vehicles-list')
    if(list != null){
        this.allVehicles = JSON.parse(list)
    }
  }

  day = new Date()
  price = 4.5


  save(){
    if(this.parkingLotForm.valid){
      let vehicle = this.parkingLotForm.value.vehicleParking
      let plate = this.parkingLotForm.value.plateParking
      let driver = this.parkingLotForm.value.driverParking
      let phone = this.parkingLotForm.value.phoneParking
    
      let vehicleParkingLot = new ParkingLot(vehicle, plate, driver, phone)
      this.allVehicles.push(vehicleParkingLot)
      localStorage.setItem('vehicles-list', JSON.stringify(this.allVehicles));


      this.parkingLotForm.reset();
    }
  }
  end(){
    let endTime = this.parkingLotForm.value.Date
    let price = this.parkingLotForm
  }
}
