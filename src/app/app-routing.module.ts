import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TelaAComponent } from './tela-a/tela-a.component';
import { TelaBComponent } from './tela-b/tela-b.component';

const routes: Routes = [
    { path: 'register-parking', component: TelaAComponent },
    { path: 'list-parking', component: TelaBComponent },
    { path: '', pathMatch: 'full', redirectTo: "/register-parking"},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }