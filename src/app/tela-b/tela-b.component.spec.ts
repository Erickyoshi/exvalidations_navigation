import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TelaBComponent } from './tela-b.component';

describe('TelaBComponent', () => {
  let component: TelaBComponent;
  let fixture: ComponentFixture<TelaBComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TelaBComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TelaBComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
